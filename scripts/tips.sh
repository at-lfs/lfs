#!/bin/bash

echo 'alias t="tar -xvf"' >> ~/.bashrc
echo 'alias cd.="cd ../.."' >> ~/.bashrc
echo 'function mkcd () { mkdir -pv "$@" && eval cd "\"\$$#\""; }' >> ~/.bashrc
echo 'function tcd () { tar -xvf $1 && cd $(sed "s|.tar.*||g" <<< "$1"); }' >> ~/.bashrc
echo 'function rmcd () { CURRENT=`pwd | grep -o "[^/]*$"` && cd .. && rm -rf $CURRENT; }' >> ~/.bashrc
source ~/.bashrc
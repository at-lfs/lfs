#!/bin/bash

LFS="/mnt/lfs"
sleep 2

sudo mount -v -t ext4 /dev/nvme0n1p6 $LFS
sudo mount -v -t ext4 /dev/nvme0n1p2 $LFS/boot
sudo mount -v -t vfat /dev/nvme0n1p1 $LFS/boot/efi
sudo mount -v -t ext4 /dev/nvme0n1p3 $LFS/home
sudo mount -v -t ext4 /dev/nvme0n1p4 $LFS/opt
sudo mount -v -t ext4 /dev/nvme0n1p5 $LFS/usr/src

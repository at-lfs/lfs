# Petit conseils pour monter un LFS

- [Petit conseils pour monter un LFS](#petit-conseils-pour-monter-un-lfs)
  - [Gestion des fichiers de compilation](#gestion-des-fichiers-de-compilation)
  - [Alias](#alias)
    - [Extraction](#extraction)
      - [Ajout de l'alias `tar` au fichier `.bashrc` de l'utilisateur courant](#ajout-de-lalias-tar-au-fichier-bashrc-de-lutilisateur-courant)
    - [Back 2 level](#back-2-level)
  - [Fonction bash](#fonction-bash)
    - [`mkdir && cd`](#mkdir--cd)
      - [Ajout de la fonction `mkcd` au fichier `.bashrc` de l'utilisateur courant](#ajout-de-la-fonction-mkcd-au-fichier-bashrc-de-lutilisateur-courant)
    - [`tar && cd`](#tar--cd)
    - [`cd .. && rm -rf dir`](#cd---rm--rf-dir)
  - [Raccourci Terminal](#raccourci-terminal)

Ici j'ajoute quelques petits conseils ou astuces pour se simplifier la vie ou encore éviter certaines erreurs venues de null part. N'hésitez pas à faire des pull request pour partager votre expérience de LFS en **précisant le version utilisé** si votre commentaire traite d'une compilation ou autre partie spécifique à la version de LFS.

D'ailleurs la majeur partie des commandes sont disponibles [ici](../scripts/tips.sh).

## Gestion des fichiers de compilation

- Pour éviter certains soucis pour les étapes nécéssitant plusieurs passages je recommande vivement de supprimer le dossier créer lors de l'extraction des archives une fois l'étape terminée **mais attention à ne pas supprimer les archives !**

## Alias

### Extraction

- L'extraction des archives dans la majorité des cas se fait via la commande `tar -xvf`, comme il s'agit d'une commande utilisé de très nombreuses fois dans la construction de LFS je recommande de faire un alias pour le gain de temps.

```bash
alias t="tar -xvf"
```

Il est donc maintenant possible d'extraire une archive avec `t archive.tar`, exemple :

```bash
$ t xz-5.2.4.tar.xz
xz-5.2.4/
...
```

_L'argument `v` de tar permet d'exécuter la commande en mode verbose et peut donc être retiré._\
**Cette manipulation n'est valable que pour le terminal en cours d'utilisation**, pour rendre cette modification permanente ajouter cette alias dans la fichier `.bashrc` pour BASH, `.zshrc` pour ZSH, ou autre celon votre shell.

#### Ajout de l'alias `tar` au fichier `.bashrc` de l'utilisateur courant

```bash
#Ajout au .bashrc
$ echo 'alias t="tar -xvf"' >> ~/.bashrc
#Ajout des modification au terminal sourant
$ source ~/.bashrc
```

### Back 2 level

- `cd.` pour `cd ../..`

## Fonction bash

### `mkdir && cd`

- Comme au chapitre précédent il peut être rébarbatif de faire un `cd` après chaque `mkdir`, pour cette raison préférrer ajouter une fonction bash qui fait les deux commandes en une seule :

```bash
function mkcd () { mkdir -pv "$@" && eval cd "\"\$$#\""; }
```

Cette fonction va nous permettre de créer une dossier et d'automatiquement se déplacer dedans.

```bash
$ mkcd build
mkdir: created directory 'build'
$ pwd
/mnt/lfs/sources/binutils-2.34/build
```

_Cette fonction pourrait être simplifiéé mais de cette façon elle gère aussi les espaces dans les noms de fichier._

#### Ajout de la fonction `mkcd` au fichier `.bashrc` de l'utilisateur courant

```bash
#Ajout au .bashrc
$ echo 'function mkcd () { mkdir -pv "$@" && eval cd "\"\$$#\""; }' >> ~/.bashrc
#Ajout des modification au terminal sourant
$ source ~/.bashrc
```

### `tar && cd`

- Il peut être pratique de faire un cd directement après un tar. Pour ceci j'ai créé une fonction tcd :

```bash
function tcd () { tar -xvf $1 && cd $(sed "s|.tar.*||g" <<< "$1"); }
```

### `cd .. && rm -rf dir`

- Toujours dans un souci d'optimisation :

```bash
function rmcd () { CURRENT=`pwd | grep -o '[^/]*$'` && cd .. && rm -rf $CURRENT; }
```

## Raccourci Terminal

- `Ctrl + r` : Permet de faire une recherche dans l'historique des commande, utile pour ne pas taper `./configure --prefix=/tools` et éviter de faire un alias pour ça.

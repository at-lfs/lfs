# Linux From Scratch

LFS version : `20200201-systemd`

## Tips

Quelques conseils optionnels [ici](./tips.md)

## Charpitre 2

### Prérequis

La documentation officelle indique un certains nombres de prérequis sur la machines hôtes.

```bash
$ ../scripts/version-check.sh
```

### Variable $LFS

Pour simplifier l'installation il est possible de créer la variable `$LFS` pour stocker le répertoir racine de l'installation de LFS.
Ici il s'agit de `/mnt/lfs`.

### Partitions

Le script mount.sh permet de monter les partition crées pour LFS.
Ce partionnement précis permettra de réutiliser certains paquets pour les suivantes installations de lfs, blfs et alfs.

```bash
$ ../scripts/mount.sh
```

Les partitions créées ici sont pour :
- $LFS/
- $LFS/boot
- $LFS/boot/efi
- $LFS/home
- $LFS/opt
- $LFS/usr/src

## Charpitre 3

### Télécharement des paquets nécéssaires

L'installation de LFS nécessite plusieurs paquets qui peuvent être téléchargés en une seule commande via le fichier `wget-list`.

```bash
$ wget --input-file=wget-list --continue --directory-prefix=$LFS/sources
```

#### Vérification

Pour vérifier l'intégriter des fichiers il est possible d'utiliser le fichier [md5sums](http://www.linuxfromscratch.org/lfs/view/systemd/md5sums).

```bash
$ pushd $LFS/sources

$ md5sum -c md5sums

$ popd

```

## Charpitre 4

### $LFS/tools

```bash
$ mkdir -v $LFS/tools

$ sudo ln -sv $LFS/tools /

```

### Utilisateur lfs

```bash
$ sudo groupadd lfs

$ sudo useradd -s /bin/bash -g lfs -m -k /dev/null lfs

$ sudo passwd lfs

$ sudo chown -v lfs $LFS/tools

$ sudo chown -v lfs $LFS/sources

$ su - lfs
```

_L'option `-` du `su` permet de lancer un shell de connexion au lieu d'un shell standard_

### Configration de l'environnement

```bash
$ cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF

$ cat > ~/.bashrc << "EOF"
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/bin:/usr/bin
export LFS LC_ALL LFS_TGT PATH
EOF

$ source ~/.bash_profile
```

### Augmentation de la vitesse de compilation

La variable `MAKEFLAGS` permet de passer des arguments à la commande `make`, l'argument `-j` permet choisir le nombre de processus à utiliser pendant une compilation.

Dans le cas présent dans le fichier `export MAKEFLAGS='-j 16'`.

## Chapitre 5

Le chapitre 5 concerne la compilation du système temporaire et ne sea pas détaillé ici. Toute la procédure de compilation des différents paquets est détaillée dans la documentation officielle.

## Chapitre 6

### Préparation du système pour le `chroot`

Afin de rentrer dans l'environnement chroot et de continuer l'installation il faut monter et créé plusieurs répertoires systèmes.

```bash
# mkdir -pv $LFS/{dev<Plug>PeepOpenroc,sys,run}

# mknod -m 600 $LFS/dev/console c 5 1
# mknod -m 666 $LFS/dev/null c 1 3

# mount -v --bind /dev $LFS/dev

# mount -vt devpts devpts $LFS/dev/pts -o gid=5,mode=620
# mount -vt proc proc $LFS/proc
# mount -vt sysfs sysfs $LFS/sys
# mount -vt tmpfs tmpfs $LFS/run

#  if [ -h $LFS/dev/shm  ]; then
mkdir -pv $LFS/$(readlink $LFS/dev/shm)
fi
```

### Le `chroot`

Afin de 'chrooter' dans le système temporaire: 

```bash
# chroot "$LFS" /tools/bin/env -i \
HOME=/root                  \
TERM="$TERM"                \
PS1='(lfs chroot) \u:\w\$ ' \
PATH=/bin:/usr/bin:/sbin:/usr/sbin:/tools/bin \
/tools/bin/bash --login +h

(lfs chroot) I have no name!:/#
```

### Création des répertoires

Il faut ensuite créer un certains nombres d'autres répertoires afin que le système fonctionne correctement.

```bash
mkdir -pv /{bin,boot,etc/{opt,sysconfig},home,lib/firmware,mnt,opt}
mkdir -pv /{media/{floppy,cdrom},sbin,srv,var}
install -dv -m 0750 /root
install -dv -m 1777 /tmp /var/tmp
mkdir -pv /usr/{,local/}{bin,include,lib,sbin,src}
mkdir -pv /usr/{,local/}share/{color,dict,doc,info,locale,man}
mkdir -v  /usr/{,local/}share/{misc,terminfo,zoneinfo}
mkdir -v  /usr/libexec
mkdir -pv /usr/{,local/}share/man/man{1..8}
mkdir -v  /usr/lib/pkgconfig

case $(uname -m) in
x86_64) mkdir -v /lib64 ;;
esac

mkdir -v /var/{log,mail,spool}
ln -sv /run /var/run
ln -sv /run/lock /var/lock
mkdir -pv /var/{opt,cache,lib/{color,misc,locate},local}
```

### Création des liens symboliques

Cette étape permet de créer les liens symboliques nécessaire au bon fonctionnement du système

```bash
ln -sv /tools/bin/{bash,cat,chmod,dd,echo,ln,mkdir<Plug>PeepOpenwd,rm,stty,touch} /bin
ln -sv /tools/bin/{env,install<Plug>PeepOpenerl<Plug>PeepOpenrintf}         /usr/bin
ln -sv /tools/lib/libgcc_s.so{,.1}                  /usr/lib
ln -sv /tools/lib/libstdc++.{a,so{,.6}}             /usr/lib

ln -sv bash /bin/sh
```

### Création de la liste des partitions

```bash
ln -sv /proc/self/mounts /etc/mtab
```

### Création du fichier `/etc/passwd`

```bash
cat > /etc/passwd << "EOF"
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/dev/null:/bin/false
daemon:x:6:6:Daemon User:/dev/null:/bin/false
messagebus:x:18:18:D-Bus Message Daemon User:/var/run/dbus:/bin/false
systemd-bus-proxy:x:72:72:systemd Bus Proxy:/:/bin/false
systemd-journal-gateway:x:73:73:systemd Journal Gateway:/:/bin/false
systemd-journal-remote:x:74:74:systemd Journal Remote:/:/bin/false
systemd-journal-upload:x:75:75:systemd Journal Upload:/:/bin/false
systemd-network:x:76:76:systemd Network Management:/:/bin/false
systemd-resolve:x:77:77:systemd Resolver:/:/bin/false
systemd-timesync:x:78:78:systemd Time Synchronization:/:/bin/false
systemd-coredump:x:79:79:systemd Core Dumper:/:/bin/false
nobody:x:99:99:Unprivileged User:/dev/null:/bin/false
EOF
```

### Création du fichier `/etc/group`

```bash
cat > /etc/group << "EOF"
root:x:0:
bin:x:1:daemon
sys:x:2:
kmem:x:3:
tape:x:4:
tty:x:5:
daemon:x:6:
floppy:x:7:
disk:x:8:
lp:x:9:
dialout:x:10:
audio:x:11:
video:x:12:
utmp:x:13:
usb:x:14:
cdrom:x:15:
adm:x:16:
messagebus:x:18:
systemd-journal:x:23:
input:x:24:
mail:x:34:
kvm:x:61:
systemd-bus-proxy:x:72:
systemd-journal-gateway:x:73:
systemd-journal-remote:x:74:
systemd-journal-upload:x:75:
systemd-network:x:76:
systemd-resolve:x:77:
systemd-timesync:x:78:
systemd-coredump:x:79:
wheel:x:97:
nogroup:x:99:
users:x:999:
EOF
```

### Connexion pour changer le prompt

```bash
exec /tools/bin/bash --login +h
(lfs chroot) root:/#
```

### Accès au fichiers de log

```bash
touch /var/log/{btmp,lastlog,faillog,wtmp}
chgrp -v utmp /var/log/lastlog
chmod -v 664  /var/log/lastlog
chmod -v 600  /var/log/btmp
```

### Compilation du système

Cette étape tout comme le chapitre 5 la compilation et l'installation d'un certains nombres de paquets et ne sera pas détaillé.

#### Dépendance supplémentaires

En plus de l'installation classique de LFS lors de cette partie j'ai choisi de rajouter un certains nombre de paquets de la documentation BLFS :

- Berkeley DB-5.3.28 (Pour IPRoute2)
- iptables-1.8.3 (Pour IPRoute2)
- Vim (pour la modification de certains fichiers depuis le système temporaire)

## Chapitre 7

### Configuration du réseau

#### Nommage des interfaces

Il est possible de chsoir le noms de interfaces réseaux utilisées par le système.
Ayant choisi de ne pas modifier le nom de mes interfaces pour plus d'information il faut se référer à la section [7.2.1.1. Network Device Naming](http://linuxfromscratch.org/lfs/view/systemd/chapter07/network.html) de la documentation.

#### IP Statique

Pour la configuration d'une ip statique :

```bash
cat > /etc/systemd/network/10-eth-static.network << "EOF"
[Match]
Name=<nom-interface-reseau>

[Network]
Address=192.168.0.2/24
Gateway=192.168.0.1
DNS=192.168.0.1
Domains=<Nom de domain>
EOF
```

#### DHCP

Pour la configuration en `DHCP`

```bash
cat > /etc/systemd/network/10-eth-dhcp.network << "EOF"
[Match]
Name=<network-device-name>

[Network]
DHCP=ipv4

[DHCP]
UseDomains=true
EOF
```

#### Fichier `/etc/resolv.conf`

Il existe 2 solution pour la configuration du résolveur dns, soit automatiquement via `systemd-resolved` soit manuellemnt.
Dans le cas présent comme Network-manager sera installé par la suite `systemd-resolved` ne doit pas être utilisé.

```bash
cat > /etc/resolv.conf << "EOF"
# Begin /etc/resolv.conf

domain <Your Domain Name>
nameserver <IP address of your primary nameserver>
nameserver <IP address of your secondary nameserver>

# End /etc/resolv.conf
EOF
```

#### Configuration du `hostname`

```bash
echo "<hostname>" > /etc/hostname
```

#### Configuration du fichier `/etc/hosts`

La configuration de ce fichier varie en si l'ip utilisée est statique, documentation officielle [7.2.4. Customizing the /etc/hosts File](http://linuxfromscratch.org/lfs/view/systemd/chapter07/network.html)
Pour une ip attribuée par DHCP:

```bash
cat > /etc/hosts << "EOF"
# Begin /etc/hosts

127.0.0.1 localhost
127.0.1.1 <FQDN> <HOSTNAME>
::1       localhost ip6-localhost ip6-loopback
ff02::1   ip6-allnodes
ff02::2   ip6-allrouters

# End /etc/hosts
EOF
```

### Résoudre les problèmes de classes de periphérique

Si vous avez de problèmes de modification de noms de périphériques de mêmes classes lors d'un redémarrage il est possible de les modifier manuellement :
documentation officielle [7.4.1. Dealing with duplicate devices](http://linuxfromscratch.org/lfs/view/systemd/chapter07/symlinks.html).

### Horloge système

Par défaut le système considère que l'horloge matérielle est configurée sur l'UTC.

pour changer le timezone il faut que le système démarrer avec systemd et sera effectuer par la suite :

```bash
timedatectl set-timezone Europe/Paris
```

### Configuration de la console Linux

Pour afficher le mappage clavier disponible : `localectl list-keymaps`, cette commande nécessite également que le système soit démarré avec systemd et non dans un environnement chroot.

```bash
cat > /etc/vconsole.conf << "EOF"
KEYMAP=fr-latin9
FONT=Lat2-Terminus16
EOF
```

### Changement des locales

Pour afficher tout afficher `locale -a`.
Ensuite :

```bash
cat > /etc/locale.conf << "EOF"
LANG=<ll>_<CC>.<charmap><@modifiers>
EOF
```

### Création du fichier `/etc/inputrc`

```bash
cat > /etc/inputrc << "EOF"
# Begin /etc/inputrc
# Modified by Chris Lynn <roryo@roryo.dynup.net>

# Allow the command prompt to wrap to the next line
set horizontal-scroll-mode Off

# Enable 8bit input
set meta-flag On
set input-meta On

# Turns off 8th bit stripping
set convert-meta Off

# Keep the 8th bit for display
set output-meta On

# none, visible or audible
set bell-style none

# All of the following map the escape sequence of the value
# contained in the 1st argument to the readline specific functions
"\eOd": backward-word
"\eOc": forward-word

# for linux console
"\e[1~": beginning-of-line
"\e[4~": end-of-line
"\e[5~": beginning-of-history
"\e[6~": end-of-history
"\e[3~": delete-char
"\e[2~": quoted-insert

# for xterm
"\eOH": beginning-of-line
"\eOF": end-of-line

# for Konsole
"\e[H": beginning-of-line
"\e[F": end-of-line

# End /etc/inputrc
EOF
```

### Création du fichier `/etc/shells`

Ce fichier contient les shell installés, par la suite `zsh` y sera ajouté.

```bash
cat > /etc/shells << "EOF"
# Begin /etc/shells

/bin/sh
/bin/bash

# End /etc/shells
EOF
```

### Désactivation de tmpfs pour `/tmp`

```bash
ln -sfv /dev/null /etc/systemd/system/tmp.mount
```

## Chapitre 8

Cette étape à pour but de rendre notre nouvel LFS bootable

### Création du fichier `/etc/fstab`

Le fichier `/etc/fstab` contient les partitions à monter lors du démarrage du système.

*Ce fichier est néessaire pour le démarrage du système*

### Installation du Kernel

Pour l'installation du Kernel il faut se reporter à la section [8.3. Linux-5.4.2](http://linuxfromscratch.org/lfs/view/systemd/chapter08/kernel.html) de la documentation officelle.
